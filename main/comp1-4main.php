<!DOCTYPE html>
<html lang="en">

<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta charset="utf-8">
<title>Competency 1</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

</head>


<body class="wholepage">


<div class="topnav" id="navjump">
	<a class="active" href="#comp1jump">Competency 1</a> 
	<a href="#comp2jump">Competency 2</a> 
	<a href="#comp3jump">Competency 3</a> 
	<a href="#comp4jump">Competency 4</a>
  		<div class="topnav-right">
    		<a href="#search">Design</a>
    		<a href="#abouta">About</a>
  		</div>
	<br>
</div>

<br>








<!--UNIT 1 HEADER--------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<header>
<h1 class="competency" id="comp1jump">Competency 1</h1>
</header>


<br>

<div class="unit" id="unit1div";> <!-- START UNIT 1 DIV-->

	<div class="section" id="1.2">
		<div class="subsection1" >
		<a href="1/comp1.2.php">Lab 1.2</a>
		</div>
		<div class="subsection2" >
		1.1 Use Proper PHP Syntax 
		<br>
		1.2 Create a Script
		</div>
	</div> 
	<br>
	<div class="section" id="1.3">
		<div class="subsection1" >
		<a href="1/comp1.3-1.4.php">Lab 1.3-1.4</a> 
		</div>
		<div class="subsection2" >
		1.4 Use appropriate data types
		</div>
	</div> 
	<br>
	<div class="section" id="1.5">
		<div class="subsection1" >
		<a href="1/comp1.5-1.6.php">Lab 1.5-1.6</a>
		</div> 
		<div class="subsection2" >
		1.5 Write PHP statements
		<br>
		1.6 Code operations and expressions
		</div>
	</div> 
	<br>
	<div class="section" id="1.7">
		<div class="subsection1" >
		<a href="1/comp1.7.php">Lab 1.7</a> 
		</div>
		<div class="subsection2" >
		1.7 Handle Conditions
		</div>
	</div>
	<br>
	<div class="section" id="1.8">
		<div class="subsection1" >
		<a href="1/comp1.8.php">Lab 1.8</a> 
		</div>
		<div class="subsection2" >
		1.8 Handle Loops
		</div>
	</div>
	<br>

	<div class="section" id="1A">
		<div class="subsection1" >
		<a href="1/comp1assmtAmod.php">Assessment Part A</a>
		</div>
		<div class="subsection2" >
		<em>Recap 1.1 - 1.8</em>
		</div>
	</div>
	<br>

	<div class="section" id="1.9">
		<div class="subsection1" >
		<a href="1/comp1.9-1.10script.php">Lab 1.9-1.10</a>
		</div>
		<div class="subsection2" > 
		1.9 Write functions
		<br>
		1.10 User server-side includes
		</div>
	</div>
	<br>
	<div class="section" id="1.11">
		<div class="subsection1" >
		<a href="1/comp1.11-1.12script.php">Lab 1.11-1.12</a>
		</div>
		<div class="subsection2" >	
		1.11 Create and utilize a numeric array
		<br>
		1.12 Create and utilize an associative array
		</div>
	</div>
	<br>
	<div class="section" id="1B">
		<div class="subsection1" >
		<a href="1/comp1assmtBscript.php">Assessment Part B</a>
		</div>
		<div class="subsection2" >
		<em>Recap 1.9 - 1.12</em>
		</div>
	</div>
	<br>
	<div class="section" id="1.13">
		<div class="subsection1" >
		<a href="1/comp1.13.php">Lab 1.13</a>
		</div>
		<div class="subsection2" >
		<br>
		Create and utilize a multidimensional array
		</div>
	</div>
	<br>
	<div class="section" id="1C">
		<div class="subsection1" >
		<a href="1/comp1assmtCscript.php">Assessment Part C</a>
		</div>
		<div class="subsection2" >
		<br>
		<em>Recap 1.13</em>
		</div>
	</div>

</div><!--END UNIT 1 DIV-->

<br>
<a id="show-x1">Show More</a>

<br>
<br>


<!--UNIT 2 HEADER--------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<header>
<h1 class="competency" id="comp2jump">Competency 2</h1>
</header>


<div class="unit" id="unit2div"><!--START UNIT 2 DIV-->

	<div class="section" id="2.1">
		<div class="subsection1" >
		<a href="2/Comp2-3.php">Lab 2.1-2.3</a> 
		</div> 
		<div class="subsection2" >
		2.1: Display information on a web page from a PHP script
		<br><br>
		2.2: Read data from a form
		<br><br>
		2.3: Perform validation
		</div>
	</div>
	<br>
	<div class="section" id="2.4">
		<div class="subsection1" >
		<a href="2/Comp2-5.php">Lab 2.4-2.5</a> 
		</div>
		<div class="subsection2" >
		2.4: Use string functions
		<br><br>
		2.5: Create a regular expression
		</div>
	</div>
	<br>
	<div class="section" id="2A">
		<div class="subsection1" >
		<a href="2/comp2assmtAscript.php">Assessment Part A</a> 
		</div>
		<div class="subsection2" >
		<em>Recap 2.1 - 2.5</em>
		</div>
	</div>
	<br>
	<div class="section" id="2.6">
		<div class="subsection1" >
		<a href="2/Comp2-8.php">Lab 2.6-2.8</a> 
		</div>
		<div class="subsection2" >
		2.6: Get the current date and time
		<br><br>
		2.7: Format dates and times 
		<br><br>
		2.8: Convert a string to a date
		</div>
	</div>
	<br>
	<div class="section" id="2.9">
		<div class="subsection1" >
		<a href="2/Comp2-8.php">2.9: Debug PHP Scripts</a> 
		</div>
		<div class="subsection2" >
		<em>No Lab</em>
		</div>
	</div>
	<br>
	<div class="section" id="2B">
		<div class="subsection1" >
		<a href="2/comp2assmtBscript.php">Assessment Part B</a> 
		</div>
		<div class="subsection2" >
		<em>Recap 2.4 - 2.9</em>
		</div>
	</div>
	<br>

	<div class="section" id="2.10">
		<div class="subsection1" >
		<a href="2/comp2-10.php">Lab 2.10</a> 
		</div>
		<div class="subsection2" >
		2.9: Debug PHP Scripts
		</div>
	</div>
	<br>
		<div class="section" id="2C">
		<div class="subsection1" >
		<a href="2/comp2assmtCscript.php">Assessment Part C</a> 
		</div>
		<div class="subsection2" >
		<em>Recap 2.1 - 2.10</em>
		</div>
	</div>

</div><!--END UNIT 2 DIV-->

<br>
<a id="show-x2">Show More</a>


<br>
<br>


<!--UNIT 3 HEADER--------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<header>
<h1 class="competency" id="comp3jump">Competency 3</h1>
</header>





<div class="unit" id="unit3div"><!--START UNIT 3 DIV-->
	
	<div class="section" id="3.1">
		<div class="subsection1" >
		<a href="3/comp3-3.php">Lab 3.1- 3.3</a> 
		</div>
		<div class="subsection2">
		3.1: Connect to a datbase from a PHP script
		<br><br>
		3.2: Send queries to a database
		<br><br>
		3.3: Parse query results from a database and display on webpage 
		</div>
	</div>
	<br>
	<div class="section" id="3A">
		<div class="subsection1" >
		<a href="3/comp3assmtAscript.php">Assessment A</a>
		</div>
		<div class="subsection2">
		<em>Recap 3.1 - 3.3</em>
		</div>
	</div>
	<br>
	<div class="section" id="3.4">
		<div class="subsection1" >
		<a href="3/comp3-5script.php">Lab 3.4-3.5</a> 
		</div>
		<div class="subsection2">
		3.4: Insert data from a PHP script
		<br><br>
		3.5: Filter and escape user input
		</div>
	</div>
	<br>
	<div class="section" id="3.6">
		<div class="subsection1" >
		<a href=#>Lab 3-6</a> 
		</div>
		<div class="subsection2">
		3.6: Update and delete data from a PHP script
		</div>
		<br>
		<div class="subsection3">
		<a href="3/comp3-6u.php">3.6 update</a> 
		<a href="3/comp3-6d.php">3.6 delete</a> 
		<a href="3/comp3-3.php">3.3 revised</a> 
		</div>
	</div> 
	<br>
	<div class="section" id="3B">
		<div class="subsection1" >
		<a href="3/comp3-5script.php">Assessment B</a>
		</div>
		<div class="subsection2">
		<em>Recap 3.4 - 3.6</em>
		</div>
		<br>
		<div class="subsection3">
		<a href="3/comp3partb2display.php">B2-Display</a>
		<a href="3/comp3partb3insert.php">B3-Insert</a>
		<a href="3/comp3partb4update.php">B4-Update</a>
		<a href="3/comp3partb5delete.php">B5-Delete</a> 
		</div>
	</div>
	<br>
	<div class="section" id="3.7">
		<div class="subsection1" >
		<a href="3/comp3-7script.php">Lab 3.7</a> 
		</div>
		<div class="subsection2">
		3.7: Create tables from a PHP script
		</div>
	</div> 
	<br>
	<div class="section" id="3C">
		<div class="subsection1" >
		<a href="3/comp3assmtC3-7script.php">Assessment C</a> 
		</div>
		<div class="subsection2">
		<em>Recap 3.7</em>
		</div>
	</div> 
	<br>
	<div class="section" id="3.8">
		<div class="subsection1" >
		<a href="3/comp3assmtD3-8script.php">Lab 3.8</a> 
		</div>
		<div class="subsection2">
		3.8: Limit rows displayed from a database from a PHP script
		</div>
	</div> 
	<br>
	<div class="section" id="3D">
		<div class="subsection1" >
		<a href="3/comp1main.php">Assessment D</a> 
		</div>
		<div class="subsection2">
		<em>Recap 3.8</em>
		</div>
	</div> 
	<br>
	<div class="section" id="">
		<div class="subsection1" >
		<a href="sampler.php">Sampler</a> 
		</div> 
		<div class="subsection2">
		<em>sample Chinook database</em>
		</div>
	</div> 

</div><!--END UNIT 3 DIV-->

<br>
<a id="show-x3">Show More</a>


<br>
<br>


<!--UNIT 4 HEADER--------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->
<!----------------------------------------------------------------------------------------------->


<header>
<h1 class="competency" id="comp4jump">Competency 4</h1>
</header>

<div class="unit" id="unit4div"><!--START UNIT 4 DIV-->

	<div class="section" id="4.1">
		<div class="subsection1" >
		<a href="4/comp4-1script.php">Lab 4.1</a>  
		</div>
		<div class="subsection2">
		4.1: Read and write cookies
		</div>
	</div>
	<br>
	<div class="section" id="4A">
		<div class="subsection1" >
		<a href="comp1-4main.php#comp4jump">Assessment A</a> 
		</div>
		<div class="subsection2">
		<em>Recap 4.1</em>
		</div>
		<div class="subsection3" >
		<a href="4/comp4assmtAindex.php">Index</a> 
		<a href="4/comp4assmtAregister.php">Registration</a>
		</div>
	</div>
	<br>
	<div class="section" id="4.2">
		<div class="subsection1" >
		<a href="4/comp4-3script.php">Lab 4.2- 4.3</a> 
		</div>
		<div class="subsection2">
		4.2: Create a session
		<br><br>4.3: Create, use, and destroy session variables
		</div>
	</div>
	<br>
	<div class="section" id="4B">
		<div class="subsection1" >
		<a href="4/comp4assmtBindex.php">Assessment B</a> 
		</div>
		<div class="subsection2">
		<em>Recap 4.2-4.3</em>
		</div>
	</div>
	<br>
	<div class="section" id="4.4">
		<div class="subsection1" >
		<a href=#comp4jump>Lab 4.4</a>
		</div>
		<div class="subsection2">
		4.4: User authentication using cookes and sessions
		</div>
		<div class="subsection3" >
		<a href="4/comp4.4register.php">Register</a> 
		<a href="4/comp4.4login.php">Login</a> 
		<a href="4/comp4.4logout.php">Logout</a> 
		<a href="4/comp4.4restricted.php">Restricted</a> 
		</div>
	</div>
	<br>
	<div class="section" id="4C">
		<div class="subsection1" >
		<a href=#comp4jump>Assessment C</a>
		</div>
		<div class="subsection2">
		<em>Recap 4.1-4.4</em>
		</div>
		<div class="subsection3" >
		<a href="4/comp4assmtC-list.php">List</a> 
		<a href="4/comp4assmtC-register.php">Register</a> 
		<a href="4/comp4assmtC-login.php">Login</a> 
		<a href="4/comp4assmtC-logout.php">Logout</a>
		<br><br> 
		<a href="4/comp4assmtC-display.php">Display</a> 
		<a href="4/comp4assmtC-insert.php">Insert</a> 
		<a href="4/comp4assmtC-update.php">Update</a> 
		<a href="4/comp4assmtC-delete.php">Delete</a> 
		</div>
	</div>
	<br>
	<div class="section" id="">
		<div class="subsection1" >
		<a href="sampler.php">Sampler</a> 
		</div>
		<div class="subsection2">
		<em>sample class database</em>
		</div>
	</div>

</div><!--END UNIT 4 DIV-->

<br>
<a id="show-x4">Show More</a>



<div class="large">
<?php 
echo "<p>This text is generated from a php script!</p>"; 
?>
</div>



<footer>
<a class="competency" href="#navjump">back to top</a>
</footer>


<script src="comp1-4main.js"></script>
</body>
</html>